<?php

namespace App\Tests\Integration\Controller;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ScoreControllerTest extends WebTestCase
{
    /**
     * @var KernelBrowser
     */
    private $client;

    public function setUp(): void
    {
        $this->client = self::createClient();
    }

    public function testScoreEndpointWorks()
    {
        $this->client->request('GET', '/api/score?term=php');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }
}
