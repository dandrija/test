FROM trafex/alpine-nginx-php7:latest

# Install composer from the official image
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

USER root

RUN apk add --no-cache \
    shadow \
    php-iconv php-tokenizer php-xmlwriter php-pdo
    #php7-pdo_mysql php-tokenizer php-xsl php-iconv

#RUN chown -R nobody:nobody /var/www/html

ARG UID
ARG GID
RUN groupmod -g $GID nobody && \
    usermod -u $UID -g $GID nobody && \
    chown $UID:$GID /run && \
    chown -R $UID:$GID /var/log && \
    chown -R $UID:$GID /var/lib/nginx

RUN mkdir -p /.composer/cache
RUN chown -R $UID:$GID /.composer/cache

USER nobody
