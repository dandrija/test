<?php

namespace App\Request;

class GithubIssuesSearchRequest extends BaseRequest implements RequestInterface
{
    protected $method = 'GET';
    protected $url = 'https://api.github.com';
    protected $endpoint = '/search/issues';

    /**
     * GithubIssuesSearchRequest constructor.
     * @param string $searchTerm
     */
    public function __construct(string $searchTerm)
    {
        $this->queryParams = [
            'q' => $searchTerm,
        ];
    }
}