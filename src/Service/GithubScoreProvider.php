<?php

namespace App\Service;

use App\Request\GithubIssuesSearchRequest;

class GithubScoreProvider extends ScoreProvider implements ScoreProviderInterface
{
    /**
     * @param string $term
     * @return float
     */
    public function fetchScore(string $term): float
    {
        $positiveResults = $this->fetchResultsFromApi(new GithubIssuesSearchRequest($term .' rocks'));
        $negativeResults = $this->fetchResultsFromApi(new GithubIssuesSearchRequest($term .' sucks'));

        return $this->calculateScore($positiveResults['total_count'], $negativeResults['total_count']);
    }
}