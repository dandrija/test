<?php

namespace App\Handler;

use App\Request\RequestInterface;

interface RequestHandlerInterface
{
    public function handle(RequestInterface $request);
}