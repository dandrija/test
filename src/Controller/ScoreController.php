<?php

namespace App\Controller;

use App\Entity\SearchResult;
use App\Event\SearchResultFetchedEvent;
use App\Service\ScoreProviderInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ScoreController
 * @package App\Controller
 *
 * @Route(path="/api")
 */
class ScoreController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * ScoreController constructor.
     * @param EntityManagerInterface $entityManager
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EntityManagerInterface $entityManager, EventDispatcherInterface $dispatcher)
    {
        $this->entityManager = $entityManager;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param ScoreProviderInterface $scoreProvider
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @Route("/score", name="score", methods={"GET"})
     */
    public function getScoreResult(ScoreProviderInterface $scoreProvider, Request $request): JsonResponse
    {
        $term = $request->query->get('term');

        $repository = $this->entityManager->getRepository(SearchResult::class);

        $searchResult = $repository->findOneBy(['term' => $term]);

        if ($searchResult) {
            return $this->json(
                $searchResult,
                200
            );
        }

        $score = $scoreProvider->fetchScore($term);

        $searchResult = new SearchResult($term, $score);

        $this->dispatcher->dispatch(
            new SearchResultFetchedEvent($searchResult),
            SearchResultFetchedEvent::class
        );

        return $this->json(
            $searchResult,
            200
        );
    }
}