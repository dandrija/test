# Test project

## Description

To use this API check the documentation at [`/api`][1] after the project setup

## Requirements

* [PHP 7.2][4] or greater

> To run this project you must have [docker][2] and [docker-compose][3] installed

## Installation
### Step 1

Begin by cloning this repository to your machine

### Step 2

Replace __UID__ and __GID__ values in .env file with id -u and id -g output from the console, respectively ( usually 1000:1000 ).

### Step 3

Cd into the project directory and run docker-compose build to download images and start docker compose

```bash
docker-compose build
docker-compose up -d
```

### Step 4

Install composer dependencies

```bash
docker-compose exec nginx 'composer install'
```

### Step 5

Run migrations

```bash
docker-compose exec nginx './bin/console doctrine:migrations:migrate'
```

[1]: http://localhost:8080/api
[2]: https://docs.docker.com/engine
[3]: https://docs.docker.com/compose
[4]: http://php.net/releases/7_2_0.php
